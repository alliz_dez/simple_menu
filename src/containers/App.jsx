import React, { Component } from 'react';

import { newAd } from '../common/adsData';
import { AdHeader } from '../components/ads/AdHeader';
import { Footer } from '../components/ads/Footer';
import { Menu } from '../components/menu/Menu';
import { AdPage } from '../components/AdPage';
import { FavePage } from '../components/FavePage';

import './index.css';

const faveLocation = 'fave';
const myAdsLocation = 'myAds';

class App extends Component {
  state = {
    isOpen: false,
    ads: [],
    favorites: [],
    location: myAdsLocation
  };

  addNewAd = () => {
    const { ads } = this.state;
    this.setState({
      ads: [...ads, newAd]
    });
  };

  handleOpenMenu = () => this.setState({ isOpen: true });

  handleCloseMenu = () => this.setState({ isOpen: false });

  addToFavorites = index => {
    const { favorites, ads } = this.state;
    this.setState({ favorites: [...favorites, ads[index]] })
  };

  removeFromFavorites = index => {
    const { favorites } = this.state;
    const newArray = favorites.splice(index + 1, 1);
    this.setState({ favorites: newArray })
  };

  handleLocation = event => {
    this.handleCloseMenu();
    this.setState({ location: event.target.name });
  };

  handleMyAdsLocation = () => this.setState({ location: myAdsLocation });

  render() {
    const { ads, favorites, isOpen, location } = this.state;
    return (
        <div className='application'>
          { isOpen && <Menu handleClose={ this.handleCloseMenu }
                            adsLength={ ads.length }
                            faveLength={ favorites.length }
                            faveName={ faveLocation }
                            myAdsName={ myAdsLocation }
                            handleLocation={ this.handleLocation }
                            loc={ location }/>
          ||
          <div>
            <AdHeader onClick={ this.handleOpenMenu }/>
            {
              location === myAdsLocation &&
              <AdPage data={ ads }
                      handleOpenMenu={ this.handleOpenMenu }
                      addNewAd={ this.addNewAd }
                      addToFavorites={ this.addToFavorites }
                      removeFromFavorites={ this.removeFromFavorites }/>
            }
            {
              location === faveLocation &&
              <FavePage data={ favorites }
                        handleOpenMenu={ this.handleOpenMenu }
                        addToFavorites={ this.addToFavorites }
                        removeFromFavorites={ this.removeFromFavorites }
                        locateToMyAds={ this.handleMyAdsLocation }/>
            }

            <Footer/>
          </div>
          }
        </div>
    )
  }
}

export default App;