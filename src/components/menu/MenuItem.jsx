import React from 'react';
import PropTypes from 'prop-types';

export const MenuItem = props => {
  const { onClick, text, count, name, loc } = props;
  return (
      <li className='menu__item'>
        <a href='javascript:void(0)'
           name={ name } onClick={ onClick }
           className={ `link menu__link ${loc === name ? 'link_active' : ''}` }>
          { text }
          <span className='menu__count'>
          { count }
        </span>
        </a>
      </li>
  )
};

MenuItem.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string,
  count: PropTypes.number,
  name: PropTypes.string,
  loc: PropTypes.string
};