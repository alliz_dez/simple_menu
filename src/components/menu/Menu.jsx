import React from 'react';
import PropTypes from 'prop-types';

import { MenuItem } from './MenuItem';

import './index.css';

export const Menu = props => {
  const { handleLocation, handleClose, faveLength, adsLength, faveName, myAdsName, loc } = props;
  return (
      <div className='menu'>
        <header className='header menu__header'>
          <button type='button' className='btn__close' onClick={ handleClose }>
            <span>&#10006;</span>
          </button>
          <h3 className='header__title'>Moй профиль</h3>
        </header>

        <ul className='list menu__list'>
          <MenuItem text='Настройки профиля' name='empty'/>

          <MenuItem text='Избранное' count={ faveLength }
                    onClick={ handleLocation } name={ faveName } loc={ loc }/>

          <MenuItem text='Мои объявления' count={ adsLength }
                    onClick={ handleLocation } name={ myAdsName } loc={ loc }/>

          <MenuItem text='Мои сделки' сount={ 0 } name='empty'/>

          <li>
            <button type='button' className='btn btn__quit'>Выход</button>
          </li>
        </ul>
      </div>
  )
};

Menu.propTypes = {
  handleLocation: PropTypes.func,
  handleClose: PropTypes.func,
  faveLength: PropTypes.number,
  adsLength: PropTypes.number,
  faveName: PropTypes.string,
  myAdsName: PropTypes.string,
  loc: PropTypes.string
};