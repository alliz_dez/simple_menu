import React from 'react';
import PropTypes from 'prop-types';

import { FaveTitle } from './faveAds/FaveTitle';
import { AdList } from './ads/AdList';
import { FaveButton } from './faveAds/FaveButton';

const emptyCondition = <p>Здесь пока ничего нет.</p>;

export const FavePage = (props,) => {
  const { data, addToFavorites, removeFromFavorite, locateToMyAds } = props;
  const { length } = data;

  return (
      <div className='container'>
        <FaveTitle dataSize={ length }/>

        <AdList data={ data }
                onFave={ addToFavorites }
                onUnFave={ removeFromFavorite }
                emptyCondition={ emptyCondition }/>

        <FaveButton onClick={ locateToMyAds }/>
      </div>
  )
};

FavePage.propTypes = {
  data: PropTypes.array,
  addToFavorites: PropTypes.func,
  removeFromFavorites: PropTypes.func,
  locateToMyAds: PropTypes.func
};