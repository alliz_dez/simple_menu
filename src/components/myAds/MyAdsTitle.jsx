import React from 'react';
import PropTypes from 'prop-types';

export const MyAdsTitle = ({ dataSize }) =>
    <h3 className='title'>Мои объявления <span className='selected'>{ dataSize }</span></h3>;


MyAdsTitle.propTypes = {
  length: PropTypes.number
};