import React from 'react';
import PropTypes from 'prop-types';

import { addIcon, dataCondition } from '../../common/adsData';

export const MyAdsButton = ({ onClick, dataSize }) =>
    <button type='button'
            onClick={ onClick }
            className={ `btn ${dataCondition(dataSize, 'btn_big', 'btn_fixed')}` }>
        <span
            className={ `${dataCondition(dataSize, 'btn__text', 'text_hidden')}` }>
          Создать
        </span>
      { addIcon }
    </button>;

MyAdsButton.propTypes = {
  onClick: PropTypes.func,
  size: PropTypes.number
};