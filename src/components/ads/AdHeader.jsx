import React from 'react';
import PropTypes from 'prop-types';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faBars from '@fortawesome/fontawesome-free-solid/faBars';

import logo from '../../assets/logo.png';

export const AdHeader = ({ onClick }) =>
    <header className='header ad__header'>

      <FontAwesomeIcon icon={ faBars } onClick={ onClick }
                       className='header__icon'/>

      <a href='javascript:void(0)' className='menu__logo_wrap'>
        <img src={ logo } className='menu__logo'/>
      </a>
    </header>;

AdHeader.propTypes = {
  onClick: PropTypes.func
};