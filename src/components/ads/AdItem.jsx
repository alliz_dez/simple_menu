import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faHeart from '@fortawesome/fontawesome-free-solid/faHeart';

class AdItem extends Component {
  state = { isLike: false };

  like = () => {
    const { onFave, elementIndex } = this.props;
    const { isLike } = this.state;

    if (!isLike) {
      this.setState({ isLike: true })
    }
    onFave(elementIndex);
  };

  unlike = () => {
    const { onUnFave, elementIndex } = this.props;
    const { isLike } = this.state;

    if (isLike) {
      this.setState({ isLike: false })
    }
    onUnFave(elementIndex);
  };

  render() {
    const { section, date, btc, product } = this.props.element;
    const { isLike } = this.state;
    const likeCondition = isLike ? this.unlike : this.like;
    return (
        <div className='ad'>
          <div className='ad__item'>
            <span className='ad__section'>{ section }</span>

            <FontAwesomeIcon icon={ faHeart }
                             onClick={ likeCondition }
                             className={ `ad__like
                             ${isLike ? 'ad__like_yes' : 'ad__like_no'}` }/>

            <div className='fake_img'/>
            <p className='ad_product'>{ product }</p>
          </div>

          <div className='ad__text'>
            <h2 className='ad__btc'>{ btc } btc</h2>
            <span className='ad__date'>{ date }</span>
          </div>
        </div>
    )
  }
}

export default AdItem;

AdItem.propTypes = {
  element: PropTypes.object,
  onFave: PropTypes.func,
  onUnFave: PropTypes.func,
  elementIndex: PropTypes.number
};