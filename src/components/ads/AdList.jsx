import React from 'react';
import PropTypes from 'prop-types';

import { dataCondition } from '../../common/adsData';
import AdItem from './AdItem';

const adList = (data, onFave, onUnFave) =>
    data.map((ad, index) =>
        <AdItem element={ ad } elementIndex={ index }
                key={ index } onFave={ onFave } onUnFave={ onUnFave }/>
    );

export const AdList = props => {
  const { data, onFave, onUnFave, emptyCondition } = props;
  return (
      dataCondition(data && data.length || 0,
          emptyCondition, adList(data, onFave, onUnFave))
  )
};

AdList.propTypes = {
  data: PropTypes.array,
  onFave: PropTypes.func,
  onUnFave: PropTypes.func,
  emptyCondition: PropTypes.element
};