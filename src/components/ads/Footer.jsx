import React from 'react';

const links =
    ['Пользовательское соглашение', 'Справка и помощь',
      'О сервисе', 'Обратная связь', '2018 SkatTrading'];

export const Footer = () =>
    <footer className='footer'>
      <ul className='container list footer__list'>
        {
          links.map((link, index) =>
              <li className='footer__item' key={ index }>
                <a href='javascript:void(0)' className='link footer__link'>
                  { link }
                </a>
              </li>
          )
        }
      </ul>
    </footer>;