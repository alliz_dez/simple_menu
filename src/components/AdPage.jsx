import React from 'react';
import PropTypes from 'prop-types';

import { MyAdsTitle } from './myAds/MyAdsTitle';
import { AdList } from './ads/AdList';
import { MyAdsButton } from './myAds/MyAdsButton';

const emptyCondition =
    <p className='myAd__text'>У Вас пока нет объявлений, хотите создать?</p>;

export const AdPage = props => {
  const { data, addToFavorites, removeFromFavorites, addNewAd } = props;
  const { length } = data;

  return (
      <div className='container'>
        <MyAdsTitle dataSize={ length }/>
        <AdList data={ data } onFave={ addToFavorites }
                onUnFave={ removeFromFavorites }
                emptyCondition={ emptyCondition }/>
        <MyAdsButton onClick={ addNewAd } dataSize={ length }/>
      </div>
  )
};

AdPage.propTypes = {
  data: PropTypes.array,
  addToFavorites: PropTypes.func,
  removeFromFavorites: PropTypes.func,
  addAd: PropTypes.func
};