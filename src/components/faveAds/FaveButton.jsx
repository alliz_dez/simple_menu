import React from 'react';
import PropTypes from 'prop-types';

import { addIcon } from '../../common/adsData';

export const FaveButton = ({ onClick }) =>
    <button type='button'
            onClick={ onClick }
            className='btn btn_fixed'>
      { addIcon }
    </button>;

FaveButton.propTypes = {
  onClick: PropTypes.func
};