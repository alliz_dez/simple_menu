import React from 'react';
import PropTypes from 'prop-types';

export const FaveTitle = ({ dataSize }) =>
    <h3 className='title'>Избранное <span className='selected'>{ dataSize }</span>
    </h3>;

FaveTitle.propTypes = {
  length: PropTypes.number
};