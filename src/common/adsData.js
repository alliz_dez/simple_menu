import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faPlus from '@fortawesome/fontawesome-free-solid/faPlus';

export const dataCondition = (dataSize, emptyCondition, mainElement) => {
  return (dataSize === 0) ? emptyCondition : mainElement;
};

export const dateParse = () => {
  let newDate = new Date;
  const month = newDate.getMonth() + 1;
  const day = newDate.getDate();
  const year = newDate.getFullYear();

  return `${month}.${day}.${year}`;
};

export const addIcon = <FontAwesomeIcon icon={ faPlus } className='icon__add'/>;

export const newAd = {
  'section': 'Техника',
  'date': dateParse(),
  'product': 'Iphone 7s',
  'btc': 0.006
};